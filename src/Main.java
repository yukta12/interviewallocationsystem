import model.Interviewee;
import model.Interviewer;
import service.InterviewAllocationService;
import service.RegistrationService;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        RegistrationService service = new RegistrationService();

        Interviewer interviewer1 = new Interviewer("IN1", List.of("MC","PSDS"), 5, List.of(1, 2, 3, 4));
        Interviewer interviewer2 = new Interviewer("IN2", List.of("MC","PSDS"), 3, List.of(1,2,3,4));

        service.registerInterviewer(interviewer1);
        service.registerInterviewer(interviewer2);


        Interviewee interviewee1 = new Interviewee("C1", List.of(1, 2, 3, 7, 8));
        Interviewee interviewee2 = new Interviewee("C2", List.of(1, 2, 3, 7, 8));

        service.registerInterviewee(interviewee1);
        service.registerInterviewee(interviewee2);

//        Interviewer interviewer3 = new Interviewer("IN2", List.of("MC", "PSDS"), 3, List.of(1,2,3,4));
//        service.registerInterviewer(interviewer3); //should not register since IN2 is already present, should be unique


        Map<String, Interviewer> allInterviewers = service.getInterviewers();
        Map<String, Interviewee> allInterviewees = service.getInterviewees();

        InterviewAllocationService allocationService = new InterviewAllocationService(allInterviewers, allInterviewees);

        allocationService.allocate();

        allocationService.viewInterviewSchedule();
    }
}