import model.Interviewer;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import service.InterviewAllocationService;
import service.RegistrationService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
public class TestRepository {

    private final RegistrationService registrationService = new RegistrationService();

    @Test
    public void testRegisterInterviewer() {
        Interviewer interviewer = new Interviewer("Rahul", Arrays.asList("MC", "PSDS"), 5, Arrays.asList(1, 2, 3));
        registrationService.registerInterviewer(interviewer);

        Map<String, Interviewer> expectedInterviewers = new HashMap<>();
        expectedInterviewers.put("Rahul", interviewer);

        assertEquals(expectedInterviewers, registrationService.getInterviewers());
    }
    @Test
    public void testRegisterDuplicateInterviewer() {
        Interviewer interviewer1 = new Interviewer("Rahul", Arrays.asList("MC", "PSDS"), 5, Arrays.asList(1, 2, 3));
        Interviewer interviewer2 = new Interviewer("Rahul", Arrays.asList("MC", "PSDS"), 5, Arrays.asList(1, 2, 3));

        registrationService.registerInterviewer(interviewer1);
        registrationService.registerInterviewer(interviewer2);

        Map<String, Interviewer> expectedInterviewers = new HashMap<>();
        expectedInterviewers.put("Rahul", interviewer1);

        assertEquals(expectedInterviewers, registrationService.getInterviewers());
    }

}
