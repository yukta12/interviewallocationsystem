package model;

import java.util.List;
public class Interviewee {
    private String name;
    private List<Integer> slots;

    /**
     * Constructor
     * @param name
     * @param slots
     */
    public Interviewee(String name, List<Integer> slots) {
        this.name = name;
        this.slots = slots;
    }

    //getters
    public String getName() {
        return name;
    }

    public List<Integer> getSlots() {
        return slots;
    }
}
