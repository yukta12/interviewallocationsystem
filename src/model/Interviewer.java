package model;


import java.util.List;

public class Interviewer {
    private String name;
    private List<String> interviewTypes;
    private int yearsOfExp;
    private List<Integer> availableSlots;


    //Constructor
    public Interviewer(String name, List<String> interviewTypes, int yearsOfExp, List<Integer> availableSlots) {
        this.name = name;
        this.interviewTypes = interviewTypes;
        this.yearsOfExp = yearsOfExp;
        this.availableSlots = availableSlots;
    }


    //setters & getters
    public String getName() {
        return name;
    }

    public List<String> getInterviewTypes() {
        return interviewTypes;
    }

    public int getYearsOfExp() {
        return yearsOfExp;
    }

    public List<Integer> getAvailableSlots() {
        return availableSlots;
    }

    public void setAvailableSlots(List<Integer> availableSlots) {
        this.availableSlots = availableSlots;
    }
}
