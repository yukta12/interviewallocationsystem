package service;

import model.Interviewee;
import model.Interviewer;

import java.util.*;

public class InterviewAllocationService {

    private Map<String, Interviewer> interviewers;
    private Map<String, Interviewee> interviewees;
    private Map<String, List<String>> allocations;// <IntervieweeName, Interviewer>

    public InterviewAllocationService(Map<String, Interviewer> interviewers, Map<String, Interviewee> interviewees) {
        this.interviewers = interviewers;
        this.interviewees = interviewees;
        this.allocations = new HashMap<>();
    }

    public void allocate() {
        for (Interviewee interviewee : interviewees.values()) {
            boolean mcAllocated = false;
            boolean psdsAllocated = false;
            for (Integer slot : interviewee.getSlots()) {
                for (Interviewer interviewer : interviewers.values()) {
                    List<Integer> availableSlots = new ArrayList<>(interviewer.getAvailableSlots());
                    if (availableSlots.contains(slot)) {
                        for (String interviewType : interviewer.getInterviewTypes()) {
                            if (interviewType.equals("MC") && !mcAllocated &&
                                    !allocations.containsKey(interviewee.getName() + "_MC")) {
                                allocations.put(interviewee.getName() + "_MC", Arrays.asList("MC", interviewer.getName(), String.valueOf(slot)));
                                availableSlots.remove(slot); // Remove allocated slot
                                interviewer.setAvailableSlots(availableSlots);
                                mcAllocated = true;
                                break;
                            } else if (interviewType.equals("PSDS") && !psdsAllocated &&
                                    !allocations.containsKey(interviewee.getName() + "_PSDS")) {
                                allocations.put(interviewee.getName() + "_PSDS", Arrays.asList("PSDS", interviewer.getName(), String.valueOf(slot)));
                                availableSlots.remove(slot); // Remove allocated slot
                                psdsAllocated = true;
                                interviewer.setAvailableSlots(availableSlots);
                                break;
                            }
                        }
                    }
                    if (mcAllocated && psdsAllocated) {
                        break; // Exit the inner loop if both MC and PSDS interviews are allocated
                    }
                }
                if (mcAllocated && psdsAllocated) {
                    break; // Exit the outer loop if both MC and PSDS interviews are allocated
                }

            }
            if (!mcAllocated) {
                allocations.put(interviewee.getName() + "_MC",
                        Arrays.asList("MC Not Possble"));
            }
            if (!psdsAllocated) {
                allocations.put(interviewee.getName() + "_PSDS",
                        Arrays.asList("PSDS Not Possble"));
            }

        }
    }


    public void viewInterviewSchedule() {
        for (Map.Entry<String, List<String>> entry : allocations.entrySet()) {
            List<String> allocation = entry.getValue();
            System.out.println(entry.getKey() + ", " + allocation.get(0) + ", " + allocation.get(1) + ", " + allocation.get(2));
        }
    }


    private Interviewer findAvailableInterviewer(String interviewType, List<Integer> preferredSlots) {
        for (Interviewer interviewer : interviewers.values()) {
            if (interviewer.getInterviewTypes().contains(interviewType)) {
                List<Integer> availabilitySlots = interviewer.getAvailableSlots();
                for (Integer preferredSlot : preferredSlots) {
                    if (availabilitySlots.contains(preferredSlot)) {
                        return interviewer;
                    }
                }
            }
        }
        return null;
    }


}
