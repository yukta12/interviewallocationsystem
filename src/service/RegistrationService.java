package service;

import model.Interviewee;
import model.Interviewer;

import java.util.HashMap;
import java.util.Map;

public class RegistrationService {
    private Map<String, Interviewer> interviewers;
    private Map<String, Interviewee> interviewees;

    public RegistrationService() {
        this.interviewers = new HashMap<>();
        this.interviewees = new HashMap<>();
    }

    public Map<String, Interviewer> getInterviewers() {
        return interviewers;
    }

    public Map<String, Interviewee> getInterviewees() {
        return interviewees;
    }

    /**
     * Registers the interviewer
     * ps- it can throw exception later and can be handled rather than just printing
     * @param interviewer
     */
    public void registerInterviewer(Interviewer interviewer) {
        if (interviewers.containsKey(interviewer.getName())) {
            System.out.println("Interviewer with name " + interviewer.getName() + " already exists. Please choose a different name.");
        } else {
            interviewers.put(interviewer.getName(), interviewer);
            System.out.println("Interviewer with " + interviewer.getName() +" registered!");
        }
    }

    /**
     * registers the interviewee; doesn't allow any duplicates(name should be unq)
     * @param interviewee
     */

    public void registerInterviewee(Interviewee interviewee) {
        if (interviewees.containsKey(interviewee.getName())) {
            System.out.println("Interviewee with name " + interviewee.getName() + " already exists. Please choose a different name.");
        } else {
            interviewees.put(interviewee.getName(), interviewee);
            System.out.println("Interviewee with " + interviewee.getName() +" registered!");
        }
    }

}
